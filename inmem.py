
from argparse import ArgumentParser
from collections import Counter
from collections import OrderedDict


def load_and_count(path):
    prods = Counter()  # {sku:count}
    prod_trans = dict()
    trans = list()  # [(prods, by_freq=None)]
    with open(path, 'r') as data:
        filtered = 0
        for i, raw_line in enumerate(data):
            print(raw_line)
            tran = raw_line.split(' ')
            print(tran)
            if '\n' in tran[-1]:
                tran[-1] = tran[-1][:-1]
            print(tran)
            if len(tran) < 3:
                filtered += 1
                continue
            for sku in tran:
                prods[sku] += 1
                if sku not in prod_trans:
                    prod_trans[sku] = []
                prod_trans[sku].append(i - filtered)
            trans.append((tran, []))
            print(f'{i + 1}/25000')
    return trans, prods, prod_trans


def gen_ppctree(trans):
    root_node = dict(sku=None, count=0, pre=0, post=0, children=OrderedDict())
    for tran in trans:
        parent = root_node
        for prod in tran[1]:
            prod_node = parent['children'].get(prod)
            if not prod_node:
                parent['children'][prod] = dict(
                    sku=prod, count=0, pre=0, post=0, children=OrderedDict())
                prod_node = parent['children'][prod]
            prod_node['count'] += 1
            parent = prod_node
    return root_node


def gen_f1_nodelists(tree_node, pre_ord=0, post_ord=0, f1_nlists=None):
    tree_node['pre'] = pre_ord
    sku = tree_node['sku']

    if f1_nlists is None:
        f1_nlists = dict()  # {sku: set{((pre, post), count)}}

    child_count = len(tree_node['children'])
    for i, child_node in enumerate(tree_node['children'].values()):
        if sku is None:
            print(f'{i+1}/{child_count}')
        pre_ord += 1
        pre_ord, post_ord = gen_f1_nodelists(
            child_node, pre_ord, post_ord, f1_nlists)

    if sku:
        tree_node['post'] = post_ord
        if sku not in f1_nlists:
            f1_nlists[sku] = list()
        f1_nlists[sku].append(
            (tree_node['pre'], tree_node['post'], tree_node['count']))
        return pre_ord, post_ord + 1

    return f1_nlists


def gen_f2_sets(tree_node):
    f2_sets = list()
    items_list = list()
    for child_node in tree_node['children'].values():
        sets, items = gen_f2_sets(child_node)
        f2_sets.extend(sets)
        items_list.extend(items)
        if tree_node['sku']:
            for item in items:
                f2_sets.append((tree_node['sku'], item))
    if not tree_node['sku']:
        return f2_sets
    items_list.append(tree_node['sku'])
    return f2_sets, items_list


def gen_f2_nodelists(f2_sets, f1_nlists, sigma):
    f2_nlists = dict()
    for i1, i2 in f2_sets:
        print(i1, i2)
        i1_nlist = f1_nlists[i1]
        i2_nlist = f1_nlists[i2]
        i1i2_nlsit = proc_nlist(i1_nlist, i2_nlist)
        if i1i2_nlsit:
            f2_nlists[f'{i1},{i2}'] = ((i1,i2),i1i2_nlsit)
    return f2_nlists


def proc_nlist(i1_nlist, i2_nlist):
    nlist = list()
    next_j = 0

    for i in range(len(i1_nlist)):
        for j in range(next_j, len(i2_nlist)):
            if i1_nlist[i][0] < i2_nlist[j][0]:
                if i1_nlist[i][1] > i2_nlist[j][1]:
                    nlist.append((i1_nlist[i][0],
                                  i1_nlist[i][1],
                                  i2_nlist[j][2]))
                    next_j = j + 1
    for i in range(len(nlist) - 2):
        pp = nlist[i]
        if i >= len(nlist) - 2:
            break
        while pp[0] == nlist[i+1][0] and pp[1] == nlist[i+1][1]:
            nlist[i] = (pp[:2], pp[2] + nlist[i+1][2])
            nlist.pop(i+1)
            if i >= len(nlist) - 2:
                break

    return nlist


def main(path, sigma):
    trans, prods, prod_trans = load_and_count(path)
    for prod, count in prods.most_common():
        print(prod, count)
        for t_idx in prod_trans[prod]:
            if count > 1:
                trans[t_idx][1].append(prod)

    ppc_tree = gen_ppctree(trans)
    nlists = list()
    nlists.append(gen_f1_nodelists(ppc_tree))
    f2_sets = gen_f2_sets(ppc_tree)
    nlists.append(gen_f2_nodelists(f2_sets, nlists[0], sigma))
    print(nlists[1])


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-db', help='Path to .dat file')
    parser.add_argument('-s', '--sigma', help='Sigma, minimal support')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    main(args.db, args.sigma)